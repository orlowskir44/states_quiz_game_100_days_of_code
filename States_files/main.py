import turtle
from turtle import Turtle, Screen
import pandas

# Pandas:
data = pandas.read_csv('50_states.csv')
states_list = list()
# States_list:
states_list = data.state.to_list()

# Screen:
screen = Screen()

screen.title("U.S. Game")
screen.bgpic('blank_states_image.gif')

# Vars:
points = 0
len_sates_list = len(states_list)
correct_answers = list()

# Game:
while True:
    # input answer:
    answer = screen.textinput(title=f'States {points}/{len_sates_list}',
                              prompt="What is another state's name ?").title()
    # Exit:
    if answer == 'Exit':
        missing_states = [states for states in states_list if states not in correct_answers ]
        new_data = pandas.DataFrame(missing_states)
        new_data.to_csv("states_to_learn.csv")
        break
    # Check answer:
    if answer  in states_list and answer not in correct_answers:
        #
        t = Turtle()
        t.penup()
        t.hideturtle()
        #
        state_data = data[data.state == answer]
        t.goto(float(state_data.x), float(state_data.y))
        t.write(state_data.state.item(),align='center',font=('',20,''))
        points += 1
        correct_answers.append(answer)
    # Win
    if points >= len_sates_list:
        t.goto(-350,0)
        t.write('🥇YOU WON!🥇',font=('',100,''))


